package com.ram.Services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@Configuration
public class ServiceFile {
	
	public BufferedWriter ecrireTextFile() {
		BufferedWriter fileBuffer = null;
		try {
			FileWriter file= new FileWriter("hello.txt");
			 fileBuffer= new BufferedWriter(file);
			 
			 fileBuffer.write("this is an application from where you can learn suitable things");
			 fileBuffer.write("thanks !");
			 fileBuffer.close();
			 
		}catch (IOException e){
			e.printStackTrace();
		}
		return fileBuffer;
	}
	
	public BufferedWriter ecrireWordFile() {
		BufferedWriter fileBuffer = null;
		try {
			FileWriter file= new FileWriter("hello.DOCX");
			 fileBuffer= new BufferedWriter(file);
			 
			 fileBuffer.write("this is an application from where you can learn suitable things");
			 fileBuffer.write("thanks !");
			 fileBuffer.close();
			 
		}catch (IOException e){
			e.printStackTrace();
		}
		return fileBuffer;
	}
	
	public BufferedWriter ecrireExcelFile() {
		BufferedWriter fileBuffer = null;
		try {
			FileWriter file= new FileWriter("hello.xlsx");
			 fileBuffer= new BufferedWriter(file);
			 
			 fileBuffer.write("this is an application from where you can learn suitable things");
			 fileBuffer.write("thanks !");
			 fileBuffer.close();
			 
		}catch (IOException e){
			e.printStackTrace();
		}
		return fileBuffer;
	}
	
	public ResponseEntity<InputStreamResource> downloadFile() throws Exception {
		ClassPathResource resource=new ClassPathResource("hello.txt");
		File fichier=resource.getFile();
		
		FileInputStream inputStream = new FileInputStream(fichier);
		InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
		
		return ResponseEntity.ok().eTag(fichier.getName()).contentLength(fichier.length()).contentType(MediaType.ALL).body(inputStreamResource);
		
		
	}
	

}
