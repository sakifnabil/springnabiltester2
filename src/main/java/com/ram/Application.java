package com.ram;

import java.io.BufferedWriter;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ram.Services.ServiceFile;

@SpringBootApplication
public class Application implements CommandLineRunner{
	public static void main(String[] args)
	{
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("hi me");
		
		ServiceFile serviceFile= new ServiceFile();
		serviceFile.ecrireTextFile();
		serviceFile.ecrireWordFile();
		serviceFile.ecrireExcelFile();
		serviceFile.downloadFile();
		
		
	}
}
